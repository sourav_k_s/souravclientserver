#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/types.h>
#include <time.h>


int main(int argc,char *argv[])
{
int clien_desc,addr_desc,connec_desc;
struct sockaddr_in clien_obj;
char rec_buffer[500];
memset(rec_buffer,'0',sizeof(rec_buffer));
memset(&clien_obj,'0',sizeof(clien_obj));
clien_desc = socket(AF_INET,SOCK_STREAM,0);

	if(clien_desc<0)
	{
        printf("Client socket creation failed\n");
        return 1;
	}
	else
	{
	printf("Client Socket Created successfully\n");
	}

clien_obj.sin_family = AF_INET;
clien_obj.sin_port = htons(5000);

	if(argc!=2)
	{
		printf("No server ip passed as an argument\n");
		return 1;
	}
	else
	{
		addr_desc = inet_pton(AF_INET,argv[1],&clien_obj.sin_addr);
			if(addr_desc<=0)
			{
				printf("IP assignment failed\n");
				return 1;
			}
			else
			{
				printf("IP Assignment successful\n");
			}


	}


connec_desc = connect(clien_desc,(struct sockaddr *)&clien_obj,sizeof(clien_obj));

	if(connec_desc<0)
	{
	printf("Connection Stablishment failed\n");
        return 1;
	}
	else
	{
	printf("Connection stablishment Successful\n");
	}
   
 	while(read(clien_desc,rec_buffer,sizeof(rec_buffer))>0)
 		{             

		printf("\n%s",rec_buffer);
		}

}
